      <aside class="span3 mt20">
        <!-- Category-->
        <div class="sidewidt">
          <h2 class="heading2"><span>Categories</span></h2>
          <ul class="nav nav-list categories">
            <?php for($c=0;$c<$numCategory;$c++){ ?>
			<li><a href="<?php echo $catURL[$c]; ?>"><?php echo $catName[$c]; ?></a></li>
			<?php } ?>
          </ul>
        </div>
           <?php if($numVHome == TRUE){ ?>
		   <!--Best sellter-->
          <div class="sidewidt">
            <h2 class="heading2"><span>Best Seller</span></h2>
            <?php for($tr=0;$tr<5;$tr++){ ?>
			<ul class="bestseller">
			  <li>
                <img style="width:60px;height:60px;" src="<?php echo $reTumb[$tr]; ?>" alt="<?php echo str_replace($HTMLascii,$HTMLreal,$reTitle[$tr]); ?>" title="<?php echo str_replace($HTMLascii,$HTMLreal,$reTitle[$tr]); ?>">
                <a class="productname" href="<?php echo $reSingle[$tr]; ?>"> <?php echo substr($reTitle[$tr],0,17); ?>...</a>
                <?php if($reDiscPrice[$tr] == TRUE){ ?>
				<span class="price">$<?php echo $reDiscPrice[$tr]; ?></span>
				<?php }else{ ?>
				<span class="price">$<?php echo $reRealPrice[$tr]; ?></span>
				<?php } ?>
              </li>
            </ul>
			<?php } ?>
          </div>
       <!-- Tag Keyword-->
        <div class="sidewidt">
          <h2 class="heading2"><span>Tag Products</span></h2>
            <ul class="tags">
              <?php for($st=0;$st<$limTag;$st++){ ?>
			  <li><a href="<?php echo $urlTag[$st]; ?>"><i class="icon-tag"></i> <?php echo $nameTag[$st]; ?></a></li>
			  <?php } ?>
            </ul>
        </div>
		   <?php } ?>
      </aside>
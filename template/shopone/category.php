<div id="maincontainer">
  <section id="product">
    <div class="container">
	 <?php if($numVHome == TRUE){ ?>
     <!--  breadcrumb -->  
      <ul class="breadcrumb">
        <li>
          <a href="<?php echo $realDomain; ?>">Home</a>
          <span class="divider">/</span>
        </li>
		<?php if(isset($_GET['t'])){ ?>
        <li>Tag <span class="divider">/</span></li>
		<?php }else{ ?>
        <li>Category <span class="divider">/</span></li>
		<?php } ?>
        <li class="active"><?php echo $titleCat; ?></li>
      </ul>
	  <?php } ?>
      <div class="row">        
        <!-- Sidebar Start-->
        <?php include_once('sidebar.php'); ?>
        <!-- Sidebar End-->
		<?php if($numVHome == TRUE){ ?>
        <!-- Category-->
        <div class="span9">          
          <!-- Category Products-->
          <section id="category">
            <div class="row">
              <div class="span9">
               <!-- Sorting-->
                <div class="sorting well">
                  <div class="btn-group pull-right">
                    <button class="btn" id="list"><i class="icon-th-list"></i>
                    </button>
                    <button class="btn btn-orange" id="grid"><i class="icon-th icon-white"></i></button>
                  </div>
                </div>
               <!-- Category-->
                <section id="categorygrid">
                  <?php 
				  for($gr=0;$gr<$xCount;$gr++){ 
					$st = $gr*3;
					if($gr == ($xCount-1)){
					  $ls = $st+$yCount;
					}else{
					  $ls = $st+3;
					}
		          ?>
                  <ul class="thumbnails grid">
                    <?php for($c=$st;$c<$ls;$c++){ ?>
					<li class="span3">
                      <a class="prdocutname" href="<?php echo $tagSingle[$c]; ?>"><?php echo substr($tagTitle[$c],0,25); ?>..</a>
                      <div class="thumbnail">
                        <a href="<?php echo $tagSingle[$c]; ?>"><img src="<?php echo $tagTumb[$c]; ?>" alt="<?php echo str_replace($HTMLascii,$HTMLreal,$tagTitle[$c]); ?>" title="<?php echo str_replace($HTMLascii,$HTMLreal,$tagTitle[$c]); ?>"></a>
                        <div class="pricetag">
                          <span class="spiral"></span><a href="<?php echo $tagSingle[$c]; ?>" class="productcart">DETAILS</a>
                          <div class="price">
                            <?php if($tagDiscPrice[$c] == TRUE){ ?>
							<div class="pricenew">$<?php echo $tagDiscPrice[$c]; ?></div>
                            <div class="priceold">$<?php echo $tagRealPrice[$c]; ?></div>
							<?php }else{ ?>
							<div class="pricenew">$<?php echo $tagRealPrice[$c]; ?></div>
							<?php } ?>
                          </div>
                        </div>
                      </div>
                    </li>
					<?php } ?>
                  </ul>
				  <?php 
				  } 				  
				  for($gr=0;$gr<$xCount;$gr++){ 
					$st = $gr*3;
					if($gr == ($xCount-1)){
					  $ls = $st+$yCount;
					}else{
					  $ls = $st+3;
					}
		          ?>
                  <ul class="thumbnails list row">
                    <?php for($c=$st;$c<$ls;$c++){ ?>
					<li>
                      <div class="thumbnail">
                        <div class="row">
                          <div class="span3">
                            <a href="<?php echo $tagSingle[$c]; ?>"><img src="<?php echo $tagTumb[$c]; ?>" alt="<?php echo str_replace($HTMLascii,$HTMLreal,$tagTitle[$c]); ?>" title="<?php echo str_replace($HTMLascii,$HTMLreal,$tagTitle[$c]); ?>"></a>
                          </div>
                          <div class="span6">
                            <a class="prdocutname" href="<?php echo $tagSingle[$c]; ?>"><?php echo $tagTitle[$c]; ?></a>
                            <div class="pricetag" style="margin-top: 50px;">
                              <span class="spiral"></span><a href="<?php echo $tagSingle[$c]; ?>" class="productcart">DETAILS</a>
                              <div class="price">
								<?php if($tagDiscPrice[$c] == TRUE){ ?>
								<div class="pricenew">$<?php echo $tagDiscPrice[$c]; ?></div>
								<div class="priceold">$<?php echo $tagRealPrice[$c]; ?></div>
								<?php }else{ ?>
								<div class="pricenew">$<?php echo $tagRealPrice[$c]; ?></div>
								<?php } ?>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </li>
					<?php } ?>
                  </ul>
				  <?php } ?>
                  <div class="pagination pull-right">
                    <ul>
                      <?php if($noPage > 1){ ?>
					  <li><a href="<?php echo $urlPage . ($noPage-1) . $suffPerm; ?>">Prev</a></li>
					  <?php 
					  }
						for($p=1;$p<=$allCount;$p++){
							if ((($p >= $noPage - 3) && ($p <= $noPage + 3)) || ($p == 1) || ($p == $allCount)) {
								if ($p == $noPage) {
					  ?>
                      <li class="active"><a href="<?php echo $urlPage . $p . $suffPerm; ?>"><?php echo $p; ?></a></li>
					  <?php }else{ ?>
                      <li><a href="<?php echo $urlPage . $p . $suffPerm;; ?>"><?php echo $p; ?></a></li>
					  <?php 
								}
							}
						}
					  if ($noPage < $allCount) {
					  ?>
                      <li><a href="<?php echo $urlPage . ($noPage+1) . $suffPerm; ?>">Next</a></li>
					  <?php } ?>
                    </ul>
                  </div>
                </section>
              </div>
            </div>
          </section>
        </div>
		<?php } ?>
      </div>
  <?php if($numVHome == TRUE){ ?>
  <section id="related" class="row">
    <div class="container">
      <h1 class="heading1"><span class="maintext">Featured Products</span><span class="subtext"> See Our Most featured Products</span></h1>
      <ul class="thumbnails">
        <?php for($tr=10;$tr<14;$tr++){ ?>
		<li class="span3">
          <a class="prdocutname" href="<?php echo $reSingle[$tr]; ?>"><?php echo substr($reTitle[$tr],0,25); ?>..</a>
          <div class="thumbnail">
            <a href="<?php echo $reSingle[$tr]; ?>"><img src="<?php echo $reTumb[$tr]; ?>" alt="<?php echo str_replace($HTMLascii,$HTMLreal,$reTitle[$tr]); ?>" title="<?php echo str_replace($HTMLascii,$HTMLreal,$reTitle[$tr]); ?>"></a>
            <div class="pricetag">
              <span class="spiral"></span><a href="<?php echo $reSingle[$tr]; ?>" class="productcart">DETAILS</a>
              <div class="price">
                  <?php if($reDiscPrice[$tr] == TRUE){ ?>
				  <div class="pricenew">$<?php echo $reDiscPrice[$tr]; ?></div>
                  <div class="priceold">$<?php echo $reRealPrice[$tr]; ?></div>
				  <?php }else{ ?>
				  <div class="pricenew">$<?php echo $reRealPrice[$tr]; ?></div>
				  <?php } ?>
              </div>
            </div>
          </div>
        </li>
		<?php } ?>
      </ul>
    </div>
  </section>
  <?php } ?>
  
    </div>
  </section>
</div>
<!-- /maincontainer -->
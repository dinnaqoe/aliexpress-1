<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
          <title><?php echo $metaTitle; ?></title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="<?php echo $metaDesc; ?>">
		<meta name="keywords" content="<?php echo $metaKeyw; ?>">
		<meta name="author" content="<?php echo $cekDomain; ?>">
		<?php echo $metaGoogle; ?>
		<link rel="canonical" href="<?php echo $canonical; ?>" />
      <link rel="shortcut icon" href="<?php echo $dirTemplate; ?>/skin/galabigshop/favicon.ico" type="image/x-icon"/>
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
      <script type="text/javascript">
         //<![CDATA[
         var review = 'Write Your Own Review';    
             var isMobile = /iPhone|iPod|iPad|Phone|Mobile|Android|hpwos/i.test(navigator.userAgent);
             var isPhone = /iPhone|iPod|Phone|Android/i.test(navigator.userAgent);
             var product_zoom = null;
             var urlsite = '<?php echo $dirTemplate; ?>/';
             var PRODUCTSGRID_POSITION_ABSOLUTE = 'masonry';
             var AJAXCART_AUTOCLOSE = 0;
             var FREEZED_TOP_MENU = 1;
             var PRODUCTSGRID_ITEM_WIDTH = 200;
             var PRODUCTSGRID_ITEM_SPACING = 20;
             var CROSSSELL_ITEM_WIDTH = 210;
             var CROSSSELL_ITEM_SPACING = 30;
             var UPSELL_ITEM_WIDTH = 209;
             var UPSELL_ITEM_SPACING = 30;
             var DETAILS_TAB = 1;
         //]]>
      </script>
      <!--[if lt IE 7]>
      <script type="text/javascript">
         //<![CDATA[
             var BLANK_URL = '<?php echo $dirTemplate; ?>/js/blank.html';
             var BLANK_IMG = '<?php echo $dirTemplate; ?>/js/spacer.gif';
         //]]>
      </script>
      <![endif]-->
      
      <link rel="stylesheet" type="text/css" href="<?php echo $dirTemplate; ?>/skin/galabigshop/css/widgets.css" media="all"/>
      <link rel="stylesheet" type="text/css" href="<?php echo $dirTemplate; ?>/skin/galabigshop/css/em_cloudzoom.css" media="all"/>
      <link rel="stylesheet" type="text/css" href="<?php echo $dirTemplate; ?>/skin/galabigshop/css/em_variation.css" media="all"/>
      <link rel="stylesheet" type="text/css" href="<?php echo $dirTemplate; ?>/skin/galabigshop/css/lightbox.css" media="all"/>
      <link rel="stylesheet" type="text/css" href="<?php echo $dirTemplate; ?>/skin/galabigshop/css/social.css" media="all"/>
      <link rel="stylesheet" type="text/css" href="<?php echo $dirTemplate; ?>/skin/galabigshop/ajaxproducts/isotope.css" media="all"/>
      <link rel="stylesheet" type="text/css" href="<?php echo $dirTemplate; ?>/skin/galabigshop/css/styles.css" media="all"/>
      <link rel="stylesheet" type="text/css" href="<?php echo $dirTemplate; ?>/skin/galabigshop/css/em_ajaxcart/em_ajaxcart.css" media="all"/>
      <link rel="stylesheet" type="text/css" href="<?php echo $dirTemplate; ?>/skin/galabigshop/em_blog/css/styles.css" media="all"/>
      <link rel="stylesheet" type="text/css" href="<?php echo $dirTemplate; ?>/skin/galabigshop/em_megamenupro/css/menu.css" media="all"/>
      <link rel="stylesheet" type="text/css" href="<?php echo $dirTemplate; ?>/skin/galabigshop/css/em_productlabels.css" media="all"/>
      <!--<link rel="stylesheet" type="text/css" href="<?php echo $dirTemplate; ?>/skin/galabigshop/css/em_quickshop.css" media="all"/>-->
      <link rel="stylesheet" type="text/css" href="<?php echo $dirTemplate; ?>/skin/galabigshop/css/fancybox/jquery.fancybox.css" media="all"/>
      <link rel="stylesheet" type="text/css" href="<?php echo $dirTemplate; ?>/skin/galabigshop/sliderwidget/csslider.css" media="all"/>
      <link rel="stylesheet" type="text/css" href="<?php echo $dirTemplate; ?>/skin/galabigshop/em_slideshow2/css/settings.css" media="all"/>
      <link rel="stylesheet" type="text/css" href="<?php echo $dirTemplate; ?>/skin/galabigshop/em_slideshow2/css/captions.css" media="all"/>
      <link rel="stylesheet" type="text/css" href="<?php echo $dirTemplate; ?>/skin/galabigshop/emtabs/css/emtabs.css" media="all"/>
	  
      <script type="text/javascript" src="<?php echo $dirTemplate; ?>/js/prototype/prototype.js"></script>
      
      <script type="text/javascript" src="<?php echo $dirTemplate; ?>/js/prototype/validation.js"></script>
      <script type="text/javascript" src="<?php echo $dirTemplate; ?>/js/scriptaculous/builder.js"></script>
      <script type="text/javascript" src="<?php echo $dirTemplate; ?>/js/scriptaculous/effects.js"></script>
      <script type="text/javascript" src="<?php echo $dirTemplate; ?>/js/scriptaculous/dragdrop.js"></script>
      <script type="text/javascript" src="<?php echo $dirTemplate; ?>/js/scriptaculous/controls.js"></script>
      <script type="text/javascript" src="<?php echo $dirTemplate; ?>/js/scriptaculous/slider.js"></script>
      <script type="text/javascript" src="<?php echo $dirTemplate; ?>/js/varien/js.js"></script>
      <script type="text/javascript" src="<?php echo $dirTemplate; ?>/js/varien/form.js"></script>
      <script type="text/javascript" src="<?php echo $dirTemplate; ?>/js/varien/menu.js"></script>
      <script type="text/javascript" src="<?php echo $dirTemplate; ?>/js/mage/translate.js"></script>
      <script type="text/javascript" src="<?php echo $dirTemplate; ?>/js/mage/cookies.js"></script>
      <script type="text/javascript" src="<?php echo $dirTemplate; ?>/js/em/jquery-1.8.3.min.js"></script>
      <script type="text/javascript" src="<?php echo $dirTemplate; ?>/js/em/jquery-noconflict.js"></script>
      <script type="text/javascript" src="<?php echo $dirTemplate; ?>/js/em/jquery.ba-hashchange.min.js"></script>
      <script type="text/javascript" src="<?php echo $dirTemplate; ?>/js/em/jquery.hoverIntent.js"></script>
      <script type="text/javascript" src="<?php echo $dirTemplate; ?>/js/em/colorpicker.js"></script>
      <script type="text/javascript" src="<?php echo $dirTemplate; ?>/skin/galabigshop/js/jquery.social.share.2.2.min.js"></script>
      <script type="text/javascript" src="<?php echo $dirTemplate; ?>/skin/galabigshop/ajaxproducts/jquery.isotope.js"></script>
      <script type="text/javascript" src="<?php echo $dirTemplate; ?>/skin/galabigshop/ajaxproducts/jquery.infinitescroll.js"></script>
      <script type="text/javascript" src="<?php echo $dirTemplate; ?>/skin/galabigshop/ajaxproducts/behaviors/manual-trigger.js"></script>
      <script type="text/javascript" src="<?php echo $dirTemplate; ?>/skin/galabigshop/js/slide-up-down.js"></script>
      <script type="text/javascript" src="<?php echo $dirTemplate; ?>/skin/galabigshop/js/lightbox.js"></script>
      <script type="text/javascript" src="<?php echo $dirTemplate; ?>/skin/galabigshop/js/ios-orientationchange-fix.js"></script>
      <script type="text/javascript" src="<?php echo $dirTemplate; ?>/skin/galabigshop/js/selectUl.js"></script>
      <script type="text/javascript" src="<?php echo $dirTemplate; ?>/skin/galabigshop/js/cloud-zoom.1.0.2.js"></script>      
      <script type="text/javascript" src="<?php echo $dirTemplate; ?>/skin/galabigshop/js/em_ajaxcart/jquery.form.js"></script>
      <script type="text/javascript" src="<?php echo $dirTemplate; ?>/skin/galabigshop/js/em_ajaxcart/lightbox.js"></script>
      <script type="text/javascript" src="<?php echo $dirTemplate; ?>/skin/galabigshop/js/em_ajaxcart/em_ajaxcart.js"></script>
      <script type="text/javascript" src="<?php echo $dirTemplate; ?>/skin/galabigshop/em_megamenupro/js/menu.js"></script>
      <script type="text/javascript" src="<?php echo $dirTemplate; ?>/skin/galabigshop/js/fancybox/jquery.fancybox.js"></script>
      <script type="text/javascript" src="<?php echo $dirTemplate; ?>/skin/galabigshop/js/fancybox/jquery.mousewheel-3.0.6.pack.js"></script>
      <!--<script type="text/javascript" src="<?php echo $dirTemplate; ?>/skin/galabigshop/js/em_quickshop.js"></script>-->
      <script type="text/javascript" src="<?php echo $dirTemplate; ?>/skin/galabigshop/sliderwidget/hammer.js"></script>
      <script type="text/javascript" src="<?php echo $dirTemplate; ?>/skin/galabigshop/sliderwidget/csslider_1.1.js"></script>
      <script type="text/javascript" src="<?php echo $dirTemplate; ?>/skin/galabigshop/em_slideshow2/js/jquery.themepunch.plugins.min.js"></script>
      <script type="text/javascript" src="<?php echo $dirTemplate; ?>/skin/galabigshop/em_slideshow2/js/jquery.themepunch.revolution.min.js"></script>
      <script type="text/javascript" src="<?php echo $dirTemplate; ?>/skin/galabigshop/emtabs/js/emtabs.js"></script>
	  <script type="text/javascript">
         var ADAPT_CONFIG = {
           path: "<?php echo $dirTemplate; ?>/skin/galabigshop/css/",
           dynamic: true,
           range: [
             '0px    to 760px  = mobile.css',
             '760px  to 980px  = 720.css',
             '980px  to 1200px = 960.css',
             '1200px   		  = 1200.css'
           ]
         };
      </script>
	  <script type="text/javascript" src="<?php echo $dirTemplate; ?>/skin/galabigshop/js/bigshop.js"></script>	  
	  <script type="text/javascript" src="<?php echo $dirTemplate; ?>/skin/galabigshop/js/adapt.js"></script>
	  <script type="text/javascript">
        //<![CDATA[
        Mage.Cookies.path     = '';
        Mage.Cookies.domain   = '';
        //]]>
      </script>      
      <!--[if lt IE 8]>
      <link rel="stylesheet" type="text/css" href="<?php echo $dirTemplate; ?>/skin/default/css/styles-ie.css" media="all" />
      <script type="text/javascript" src="<?php echo $dirTemplate; ?>/skin/galabigshop/em_megamenupro/js/ie7.js"></script>
      <![endif]-->
      <!--[if lt IE 7]>
      <script type="text/javascript" src="<?php echo $dirTemplate; ?>/js/lib/ds-sleight.js"></script>
      <script type="text/javascript" src="<?php echo $dirTemplate; ?>/skin/galabigshop/js/ie6.js"></script>
      <![endif]-->
      <link rel="stylesheet" id="emcssvariation" type="text/css" media="all" href="<?php echo $dirTemplate; ?>/skin/galabigshop/css/theme.css"/>
      <!--[if lt IE 9]>
      <script type="text/javascript">
         (function() {
         	var el = document.getElementById('emcssvariation');
         	el.parentNode.removeChild(el);
         })();
      </script>
      <link rel="stylesheet" id="emcssvariation" type="text/css" media="all" href="<?php echo $dirTemplate; ?>/skin/galabigshop/css/theme.css"/>
      <![endif]-->
      <link href="http://fonts.googleapis.com/css?family=Roboto:100,200,300,400,500,600,700,800,900,1000&amp;subset=latin,cyrillic-ext,cyrillic,greek-ext,greek,vietnamese,latin-ext" rel="stylesheet" type="text/css"/>
      <style type="text/css"></style>
      <script type="text/javascript">
         //<![CDATA[
         var urlSkinsite = '<?php echo $dirTemplate; ?>/skin/galabigshop/';
         LightboxOptions = Object.extend({
             fileLoadingImage:        urlSkinsite+'images/loading.gif',     
             fileBottomNavCloseImage: urlSkinsite+'images/closelabel.png',
         
             overlayOpacity: 0.8,   // controls transparency of shadow overlay
         
             animate: true,         // toggles resizing animations
             resizeSpeed: 7,        // controls the speed of the image resizing animations (1=slowest and 10=fastest)
         
             borderSize: 10,         //if you adjust the padding in the CSS, you will need to update this variable
         
         	// When grouping images this is used to write: Image # of #.
         	// Change it for non-english localization
         	labelImage: "Image",
         	labelOf: "of"
         }, window.LightboxOptions || {});
         //]]>
      </script>
      <script type="text/javascript">
         //<![CDATA[
         optionalZipCountries = ["HK","IE","MO","PA"];
         //]]>
      </script>
      <script type="text/javascript">
         //<![CDATA[
         		
         	/*if (typeof EM == 'undefined') EM = {};
			
         	EM.QuickShop = {
         		BASE_URL : '<?php echo $dirTemplate; ?>/',
         		QS_FRM_TYPE : 1,
         		QS_FRM_WIDTH : 900,
         		QS_FRM_HEIGHT : 650,
         		QS_TEXT: 'QUICK SHOP',
         		QS_BTN_WIDTH : 96,
         		QS_BTN_HEIGHT : 25
         	};
         	if(EM.QuickShop.QS_FRM_TYPE == 0 ){
         		EM.QuickShop.QS_FRM_WIDTH = EM.QuickShop.QS_FRM_WIDTH + "%";
         		EM.QuickShop.QS_FRM_HEIGHT = EM.QuickShop.QS_FRM_HEIGHT + "%";
         	}*/
         
         //]]	
      </script> 
      <script type="text/javascript">//<![CDATA[
         var Translator = new Translate([]);
         //]]>
      </script>
   </head>            
   <body class="cms-index-index cms-galabigshop-home adapt-3">
      <div class="wrapper">
         <div class="page <?php if(isset($_GET['i'])){echo 'two-columns-right';}else{echo 'one-column';}?>">
            <div class="wrapper_header">
               <div class="">
                  <div class="header-container">
                     <div class="header-top">
                        <div class="container_24">
                           <div class="inner_top">
                              <div class="grid_24">
                                 <div class="header-top-left">
                                    <p class="welcome-msg">Default welcome msg! </p>
                                    <ul class="links">
                                       <li class="login_login"><a title="Log In" onclick="affFunction()" href="#">Log In</a></li>
                                       <li class="login_signup"><span> or </span><a title="Register" onclick="affFunction()" href="#">Register</a></li>
                                    </ul>
                                 </div>
                                 <div class="header-top-right">
                                    <ul class="links">
                                       <li class="first"><a href="#" onclick="affFunction()" title="My Account">My Account</a></li>
                                       <li class=" last"><a href="#" onclick="affFunction()" title="My Wishlist">My Wishlist</a></li>
                                    </ul>
                                    <div class="form-language">
                                       <ul>
                                          <li class="selected	 first"><a href="#" onclick="affFunction()" title="English">En</a></li>
                                          <li class=""><a href="#" onclick="affFunction()" title="French">Fr</a></li>
                                          <li class="last"><a href="#" onclick="affFunction()" title="German">Ge</a></li>
                                       </ul>
                                       <script type="text/javascript">decorateGeneric($$('.form-language ul li'), ['first','last'])</script>
                                    </div>
                                    <div class="block block-currency">
                                       <div class="block-content">
                                          <ul class="select-curency">
                                             <li class="first">
                                                <a href="#" onclick="affFunction()" title="Australian Dollar">£</a>
                                             </li>
                                             <li class="">
                                                <a href="#" onclick="affFunction()" title="Euro">€</a>
                                             </li>
                                             <li class="selected last">
                                                <a href="#" onclick="affFunction()" title="US Dollar">$</a>
                                             </li>
                                          </ul>
                                       </div>
                                       <script type="text/javascript">decorateGeneric($$('.block-currency .block-content ul li'), ['first','last'])</script>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="header-bottom">
                        <div class="container_24">
                           <div class="inner_bottom">
                              <div class="grid_24">
                                 <div class="header-bottom-left">
                                    <a href="<?php echo $realDomain; ?>" title="GalaBigShop  Theme"  class="logo"><strong>BigShop </strong>
									<img src="<?php echo $dirTemplate; ?>/skin/galabigshop/images/logo.png" alt="<?php echo $webtitle; ?>" title="<?php echo $webtitle; ?>"></a>
                                 </div>
                                 <div class="header-bottom-middle">
                                    <form id="search_mini_form" action="" method="post">
                                       <div class="form-search">
                                          <div class="input_cat">
                                            
                                          </div>
                                          <div class="input_search">
                                             <input name="search" id="search" type="text" name="q" value="" class="input-text" maxlength="128" autocomplete="off">
                                             <button type="submit" title="Search" class="button"><span><span>Search</span></span></button>
                                             <div id="search_autocomplete" class="search-autocomplete" style="display: none;"></div>
                                             <script type="text/javascript">
                                                //<![CDATA[
                                                    var searchForm = new Varien.searchForm('search_mini_form', 'search', 'Enter keyword');
                                                //]]>
                                             </script>
                                          </div>
                                       </div>
                                    </form>
                                 </div>
                                 <div class="header-bottom-right">
                                    <div class="dropdown-cart no_quickshop top-cart-inner" id="click">
									   <div class="summary block-title link-top-cart" id="link-top-cart">
										  <a id="topcartlink" class="amount" href="#" onclick="affFunction()"><span>My Cart</span></a>
										  <span class="arrow-box">&nbsp;</span>
									   </div>
									   <div class="top-cart-content" id="top_cart" style="display: none;">
										  <div class="block block-cart-top">
											 <div class="block-content">
												<ol id="cart-sidebar" class="mini-products-list">
												   
												</ol>
												<div class="summary">
												   <div class="subtotal">
													  <p class="amount"><a href="#" onclick="affFunction()">0 items</a></p>
													  <span class="price">$0</span>                                
													  <div class="price-tax"></div>
												   </div>
												</div>
												<div class="actions">
												   <button type="button" title="Checkout" class="button"  onclick="affFunction()"><span><span>Checkout</span></span></button>	
												   <p class="go-cart"><a onclick="affFunction()" href="#"><span>Go to Shopping Cart</span></a></p>
												</div>
											 </div>
										  </div>
									   </div>
									</div>
									<script type="text/javascript">//<![CDATA[
										jQuery(function($) {	   
										   var container = $("#top_cart");
											if(isMobile == true){
												$('#topcartlink').attr('href','javascript:void(0)');
												$("#link-top-cart").click(
													function( event ){
														container.slideToggle();
														$(this).toggleClass('click_top_cart');
													}
												);
											}else{
												// Hide Cart
												var timeout = null;
												function hideCart() {
													if (timeout)
													clearTimeout(timeout);
													timeout = setTimeout(function() {
													timeout = null;
													$('#top_cart').slideUp(300);
													$('#topcartlink').removeClass('over');
													}, 200);
												}
												
												// Show Cart
												function showCart() {				
													if (timeout)
													clearTimeout(timeout);
													timeout = setTimeout(function() {
													timeout = null;
													$('#top_cart').slideDown(300);
													$('#topcartlink').addClass('over');
													}, 200);
												}
												// Link Cart         
												 $('#link-top-cart')
												 .bind('mouseover', showCart)
												 .bind('click', showCart)
												 .bind('mouseout', hideCart);
												
												// Cart Content
												 $('#top_cart')
												 .bind('mouseover', showCart)
												 .bind('click', hideCart)
												 .bind('mouseout', hideCart);
											}
											
										});
									//]]>
									</script>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <p id="back-top" style="display: none;"><a title="Back to Top" href="#top">Back to Top</a></p>
               </div>
            </div>
            <?php

			if(isset($_GET['i'])){
				include_once('item.php');
			}elseif(isset($_GET['t']) || isset($_GET['c'])){
				include_once('category.php');
			}else{
				include_once('home.php');
			}

			?>
            
            <div class="wrapper_footer">
               <div class="container_24 ">
                  <div class="grid_24 em-footer">
                     <div class="footer-container">
                        <div class="footer">
                           <ul>
                              <li><a href="#" onclick="affFunction()">About Us</a></li>
                              <li><a href="#" onclick="affFunction()">Customer Service</a></li>
                              <li class="last privacy"><a href="#" onclick="affFunction()">Privacy Policy</a></li>
                           </ul>
                           <div class="footer-address">
                              <address>© 2013 <?php echo $realDomain; ?>. All Rights Reserved.</address>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="clear"></div>
               </div>
            </div>
            
            </div>
         </div>
      </div>
	    <script>
		function affFunction() {
			location.href = "<?php echo $affLink; ?>";
		}
		<?php if(isset($_GET['i'])){ ?>
		function affClick() {
			location.href = '<?php echo $affLink; ?>';
			window.open('<?php echo $realDomain; ?>/cart/<?php echo $itemID . $suffPerm; ?>');
		}
		<?php } ?>
		</script>
		<?php echo $histats; ?>
   </body>
</html>
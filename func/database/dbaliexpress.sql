SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `tblcategory`
-- ----------------------------
DROP TABLE IF EXISTS `tblcategory`;
CREATE TABLE `tblcategory` (
  `tblcat_id` bigint(20) NOT NULL,
  `tblcat_parent_id` bigint(20) NOT NULL,
  `tblcat_name` varchar(200) NOT NULL,
  `tblcat_aff` int(10) NOT NULL,
  `tblcat_update` date NOT NULL,
  PRIMARY KEY (`tblcat_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tblcategory
-- ----------------------------
INSERT INTO `tblcategory` VALUES ('2', '0', 'Food', '0', '2016-03-23');
INSERT INTO `tblcategory` VALUES ('5', '0', 'Electrical Equipment &amp; Supplies', '55', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('6', '13', 'Home Appliances', '0', '2016-03-20');
INSERT INTO `tblcategory` VALUES ('7', '0', 'Computer &amp; Office', '75', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('13', '0', 'Home Improvement', '68', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('15', '0', 'Home &amp; Garden', '68', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('17', '0', 'Gifts &amp; Crafts', '68', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('18', '0', 'Sports &amp; Entertainment', '68', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('21', '0', 'Office &amp; School Supplies', '75', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('23', '200003590', 'Packaging &amp; Shipping', '0', '2016-03-20');
INSERT INTO `tblcategory` VALUES ('26', '0', 'Toys &amp; Hobbies', '70', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('30', '7', 'Security &amp; Protection', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('34', '0', 'Automobiles &amp; Motorcycles', '68', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('39', '0', 'Lights &amp; Lighting', '68', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('41', '200003590', 'Mechanical Parts &amp; Fabrication Services', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('42', '13', 'Hardware', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('43', '200003590', 'Machinery', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('44', '0', 'Consumer Electronics', '55', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('66', '0', 'Health &amp; Beauty', '70', '2016-03-23');
INSERT INTO `tblcategory` VALUES ('80', '200003590', 'Rubber &amp; Plastics', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('125', '15', 'Garden Supplies', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('238', '2', 'Grain Products', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('289', '2', 'Coffee', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('322', '0', 'Shoes', '70', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('405', '15', 'Home Textile', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('502', '0', 'Electronic Components &amp; Supplies', '55', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('504', '502', 'Electronic Data Systems', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('509', '0', 'Phones &amp; Telecommunications', '75', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('515', '502', 'Electronics Stocks', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('516', '5', 'Other Electrical Equipment', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('526', '5', 'Contactors', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('527', '5', 'Professional Audio, Video &amp; Lighting', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('528', '5', 'Batteries', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('530', '39', 'Lighting Accessories', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('540', '5', 'Fuses', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('702', '7', 'Laptops', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('712', '7', 'Software', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('717', '7', 'Other Computer Products', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('1333', '13', 'Other Home Improvement', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('1417', '5', 'Power Tools', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('1420', '13', 'Tools', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('1501', '0', 'Mother &amp; Kids', '70', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('1503', '0', 'Furniture', '68', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('1504', '39', 'Ceiling Lights &amp; Fans', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('1509', '0', 'Jewelry', '70', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('1511', '0', 'Watches', '70', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('1513', '66', 'Sanitary Paper', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('1524', '0', 'Luggage &amp; Bags', '55', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('1526', '15', 'Lighters &amp; Smoking Accessories', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('1528', '1511', 'Clocks', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('1537', '200003590', 'Measurement &amp; Analysis Instruments', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('2112', '21', 'Paper', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('2115', '21', 'Other Office &amp; School Supplies', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('2202', '21', 'Books', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('2621', '26', 'Action &amp; Toy Figures', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('2829', '200003590', 'Service Equipment', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('3015', '34', 'Roadway Safety', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('3305', '66', 'Oral Hygiene', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('3306', '66', 'Skin Care', '0', '2016-03-20');
INSERT INTO `tblcategory` VALUES ('3708', '1503', 'Furniture Parts', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('3709', '1503', 'Furniture Making Machinery', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('3710', '15', 'Home Decor', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('3712', '1503', 'Furniture Accessories', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('3803', '1524', 'Coin Purses &amp; Holders', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('3999', '39', 'Other Lights &amp; Lighting Products', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('4001', '502', 'Active Components', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('4002', '502', 'Electronics Production Machinery', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('4003', '502', 'Electronic Accessories &amp; Supplies', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('4004', '502', 'Optoelectronic Displays', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('4005', '502', 'Passive Components', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('4099', '502', 'Other Electronic Components', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('4103', '5', 'Fuse Components', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('4105', '5', 'Power Distribution Equipment', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('4338', '1503', 'Woodworking Machinery Parts', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('32212', '1501', 'Children&#39;s Shoes', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('82207', '1503', 'Woodworking Adhesives', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('141902', '5', 'Generators', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('141903', '5', 'Insulation Materials &amp; Elements', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('141904', '5', 'Wires, Cables &amp; Cable Assemblies', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('141905', '5', 'Electrical Plugs &amp; Sockets', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('141906', '5', 'Switches', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('141907', '5', 'Transformers', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('141909', '5', 'Relays', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('141911', '5', 'Power Supplies', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('141913', '5', 'Power Accessories', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('141914', '5', 'Circuit Breakers', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('150301', '1503', 'Commercial Furniture', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('150302', '1503', 'Outdoor Furniture', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('150303', '1503', 'Home Furniture', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('150304', '1503', 'Office Furniture', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('150306', '1503', 'Furniture Hardware', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('150399', '1503', 'Other Furniture', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('150401', '39', 'Outdoor Lighting', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('150402', '39', 'Light Bulbs', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('150407', '502', 'Electronic Signs', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('150412', '502', 'EL Products', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('150512', '5', 'Electronic &amp; Instrument Enclosures', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('152401', '1524', 'Backpacks', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('152404', '1524', 'Luggage &amp; Travel Bags', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('152405', '1524', 'Wallets', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('152409', '1524', 'Bag Parts &amp; Accessories', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('211106', '21', 'Desk Accessories &amp; Organizer', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('211111', '21', 'Painting Supplies', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('212002', '21', 'Presentation Boards', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('361120', '1511', 'Pocket &amp; Fob Watches', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('380230', '509', 'Phone Bags &amp; Cases', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('390501', '39', 'LED Lighting', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('390503', '39', 'Portable Lighting', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('400301', '5', 'Electrical Ceramics', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('660103', '66', 'Makeup', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('660302', '66', 'Shaving &amp; Hair Removal', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('5090301', '509', 'Mobile Phones', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('14190403', '5', 'Wiring Accessories', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('14190406', '5', 'Connectors &amp; Terminals', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('39050501', '39', 'Book Lights', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('39050508', '39', 'Night Lights', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('70803003', '7', 'Mini PCs', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('100001118', '1501', 'Baby Care', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('100001193', '1503', 'Bamboo Furniture', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('100001196', '1503', 'Glass Furniture', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('100001198', '1503', 'Metal Furniture', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('100001199', '1503', 'Plastic Furniture', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('100001200', '1503', 'Rattan / Wicker Furniture', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('100001201', '1503', 'Wood Furniture', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('100001203', '1503', 'Children Furniture', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('100001204', '1503', 'Antique Furniture', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('100001205', '1503', 'Inflatable Furniture', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('100001206', '1503', 'Folding Furniture', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('100001606', '322', 'Women&#39;s Shoes', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('100001607', '322', 'Women&#39;s Boots', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('100001611', '322', 'Women&#39;s Sandals', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('100001615', '322', 'Men&#39;s Shoes', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('100001617', '322', 'Men&#39;s Boots', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('100001619', '322', 'Men&#39;s Sandals', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('100001622', '26', 'Baby Toys', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('100001623', '26', 'Outdoor Fun &amp; Sports', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('100001625', '26', 'Learning &amp; Education', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('100001626', '26', 'Classic Toys', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('100001629', '26', 'Electronic Toys', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('100001663', '26', 'Diecasts &amp; Toy Vehicles', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('100002964', '1501', 'Baby Bedding', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('100002992', '15', 'Festive &amp; Party Supplies', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('100003070', '0', 'Men&#39;s Clothing &amp; Accessories', '70', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('100003084', '100003070', 'Hoodies &amp; Sweatshirts', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('100003086', '100003070', 'Jeans', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('100003088', '100003070', 'Shorts', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('100003109', '0', 'Women&#39;s Clothing &amp; Accessories', '70', '2016-03-23');
INSERT INTO `tblcategory` VALUES ('100003141', '100003109', 'Hoodies &amp; Sweatshirts', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('100003186', '1501', 'Boys Clothing', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('100003199', '1501', 'Girls Clothing', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('100003235', '0', 'Weddings &amp; Events', '70', '2016-03-23');
INSERT INTO `tblcategory` VALUES ('100003240', '200000875', 'Stage &amp; Dance Wear', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('100003269', '100003235', 'Wedding Dresses', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('100003270', '100003235', 'Bridesmaid Dresses', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('100003745', '21', 'Notebooks &amp; Writing Pads', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('100003804', '21', 'Filing Products', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('100003809', '21', 'Office Binding Supplies', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('100003819', '21', 'Cutting Supplies', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('100003836', '21', 'Adhesives &amp; Tapes', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('100004814', '15', 'Bathroom Products', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('100004839', '200003590', 'Printing Materials', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('100004950', '15', 'Rain Gear', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('100005061', '7', 'Netbooks &amp; UMPC', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('100005062', '7', 'Tablet PCs', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('100005063', '7', 'Laptop Accessories', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('100005094', '21', 'School &amp; Educational Supplies', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('100005259', '18', 'Fitness &amp; Body Building', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('100005322', '18', 'Golf', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('100005383', '18', 'Musical Instruments', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('100005433', '18', 'Camping &amp; Hiking', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('100005444', '18', 'Fishing', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('100005460', '18', 'Horse Racing', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('100005471', '18', 'Hunting', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('100005479', '18', 'Shooting', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('100005505', '18', 'Sports Safety', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('100005575', '18', 'Water Sports', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('100005599', '18', 'Skiing &amp; Snowboarding', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('100005624', '100003235', 'Wedding Accessories', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('100005663', '18', 'Other Sports &amp; Entertainment Products', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('100005790', '100003235', 'Cocktail Dresses', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('100005791', '100003235', 'Prom Dresses', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('100005792', '100003235', 'Evening Dresses', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('100005823', '100003235', 'Mother of the Bride Dresses', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('100006182', '2', 'Tea', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('100006206', '15', 'Pet Products', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('100006479', '13', 'Kitchen &amp; Bath Fixtures', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('100006749', '1509', 'Rings', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200000084', '1511', 'Watch Accessories', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200000097', '1509', 'Bracelets &amp; Bangles', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200000109', '1509', 'Necklaces &amp; Pendants', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200000139', '1509', 'Earrings', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200000161', '1509', 'Wedding &amp; Engagement Jewelry', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200000179', '34', 'Convertible Accessoires', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200000191', '34', 'Replacement Parts', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200000293', '34', 'Lights &amp; Indicators', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200000322', '34', 'Diagnostic Tools', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200000361', '34', 'Transporting &amp; Storage', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200000369', '34', 'Car Electronics', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200000406', '34', 'RV Parts &amp; Accessories', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200000407', '34', 'ATV Parts &amp; Accessories', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200000408', '34', 'Motorcycle Accessories &amp; Parts', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200000528', '1501', 'Baby Boys Clothing', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200000567', '1501', 'Baby Girls Clothing', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200000599', '100003070', 'Accessories', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200000662', '100003070', 'Jackets &amp; Coats', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200000668', '100003070', 'Shirts', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200000673', '100003070', 'Sleep &amp; Lounge', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200000692', '100003070', 'Suits &amp; Blazers', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200000701', '100003070', 'Sweaters', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200000706', '100003070', 'Swimwear', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200000707', '100003070', 'Tops &amp; Tees', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200000708', '100003070', 'Underwear', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200000724', '100003109', 'Accessories', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200000773', '100003109', 'Intimates', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200000774', '1501', 'Maternity', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200000775', '100003109', 'Jackets &amp; Coats', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200000777', '100003109', 'Sleep &amp; Lounge', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200000781', '100003109', 'Socks &amp; Hosiery', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200000782', '100003109', 'Suits &amp; Sets', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200000783', '100003109', 'Sweaters', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200000784', '100003109', 'Swimwear', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200000785', '100003109', 'Tops &amp; Tees', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200000875', '0', 'Novelty &amp; Special Use', '68', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200001092', '100003109', 'Jumpsuits &amp; Rompers', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200001095', '18', 'Sports Clothing', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200001096', '200000875', 'World Apparel', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200001270', '200000875', 'Costumes &amp; Accessories', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200001271', '200000875', 'Exotic Apparel', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200001355', '200000875', 'Work Wear &amp; Uniforms', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200001520', '100003235', 'Wedding Party Dress', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200001553', '100003235', 'Celebrity-Inspired Dresses', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200001554', '100003235', 'Homecoming Dresses', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200001556', '100003235', 'Quinceanera Dresses', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200001648', '100003109', 'Blouses &amp; Shirts', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200002086', '15', 'Kitchen,Dining &amp; Bar', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200002101', '1501', 'Baby Shoes', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200002124', '322', 'Shoe Accessories', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200002136', '322', 'Men&#39;s Fashion Sneakers', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200002155', '322', 'Women&#39;s Flats', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200002161', '322', 'Women&#39;s Pumps', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200002164', '322', 'Women&#39;s Fashion Sneakers', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200002253', '322', 'Men&#39;s Flats', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200002255', '509', 'Other Phones &amp; Telecommunications Products', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200002283', '39', 'Novelty Lighting', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200002318', '7', 'Desktops &amp; Servers', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200002319', '7', 'Computer Components', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200002320', '7', 'Networking', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200002321', '7', 'Storage Devices', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200002342', '7', 'Computer Peripherals', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200002361', '7', 'Tablet Accessories', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200002394', '44', 'Accessories &amp; Parts', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200002395', '44', 'Camera &amp; Photo', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200002396', '44', 'Video Games', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200002397', '44', 'Home Audio &amp; Video', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200002398', '44', 'Portable Audio &amp; Video', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200002418', '2', 'Dried Fruit', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200002419', '2', 'Nut &amp; Kernel', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200002444', '66', 'Bath &amp; Shower', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200002454', '66', 'Fragrances &amp; Deodorants', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200002458', '66', 'Hair Care &amp; Styling', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200002489', '66', 'Hair Extensions &amp; Wigs', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200002496', '66', 'Health Care', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200002547', '66', 'Nails &amp; Tools', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200002569', '66', 'Tools &amp; Accessories', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200002633', '26', 'Models &amp; Building Toy', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200002636', '26', 'Novelty &amp; Gag Toys', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200002639', '26', 'Remote Control Toys', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200003009', '39', 'Commercial Lighting', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200003010', '15', 'Other Home Products', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200003013', '34', 'GPS &amp; Accessories', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200003045', '66', 'Sex Products', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200003132', '509', 'Backup Powers', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200003136', '15', 'Housekeeping &amp; Organization', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200003196', '21', 'Pens, Pencils &amp; Writing Supplies', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200003197', '21', 'Labels, Indexes &amp; Stamps', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200003198', '21', 'Calendars, Planners &amp; Cards', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200003210', '39', 'Professional Lighting', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200003225', '26', 'Dolls &amp; Stuffed Toys', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200003226', '26', 'Puzzles &amp; Magic Cubes', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200003230', '13', 'Building Supplies', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200003232', '13', 'Electrical', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200003238', '21', 'Mail &amp; Shipping Supplies', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200003292', '13', 'Basis Material', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200003410', '200000875', 'Traditional Chinese Clothing', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200003482', '100003109', 'Dresses', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200003491', '100003070', 'Socks', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200003551', '66', 'Tattoo &amp; Body Art', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200003570', '18', 'Cycling', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200003575', '39', 'LED Lamps', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200003590', '0', 'Industry &amp; Business', '75', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200003592', '1501', 'Safety', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200003594', '1501', 'Activity &amp; Gear', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200003595', '1501', 'Feeding', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200003857', '39', 'Holiday  Lighting', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200004414', '44', 'Other Consumer Electronics', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200004619', '34', 'Interior Accessories', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200004620', '34', 'Exterior Accessories', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200004621', '34', 'Tools &amp; Equipment', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200004635', '34', 'Car Care', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200004720', '7', 'Office Electronics', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200005059', '18', 'Racquet Sports', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200005101', '18', 'Entertainment', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200005143', '18', 'Roller,Skate board &amp;Scooters', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200005156', '18', 'Running', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200005194', '0', 'Travel and Vacations', '68', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200005195', '200005194', 'Public Transport Discount Cards', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200005196', '200005194', 'Rentals', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200005203', '200005194', 'Travel Discount Coupons', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200005212', '200005194', 'Travel Products', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200005276', '18', 'Sneakers', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200005280', '44', 'Electronic Cigarettes', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200005298', '509', 'Mobile Phone LCDs', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200010057', '1524', 'Men&#39;s Bags', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200010063', '1524', 'Women&#39;s Bags', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200010196', '44', 'Smart Electronics', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200040001', '15', 'House Ornamentation', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200066014', '1524', 'Kids &amp; Baby&#39;s Bags', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200068019', '1524', 'Functional Bags', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200074001', '66', 'Beauty Essentials', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200084017', '509', 'Mobile Phone Accessories', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200086021', '509', 'Mobile Phone Parts', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200094001', '18', 'Team Sports', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200114002', '2', 'Medlar', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200118008', '100003070', 'Bottoms', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200118010', '100003109', 'Bottoms', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200126001', '509', 'Communication Equipments', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200130003', '509', 'Mobile Phone Touch Panel', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200132001', '1509', 'Jewelry Sets &amp; More', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200154001', '15', 'Arts,Crafts &amp; Sewing', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200154003', '1509', 'Beads &amp; Jewelry Making', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200166001', '1501', 'Family Matching Outfits', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200182006', '7', 'PC Tablets', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200188001', '1509', 'Fine Jewelry', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200214006', '1511', 'Men&#39;s Watches', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200214031', '1511', 'Women&#39;s Watches', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200214033', '39', 'Lamps &amp; Shades', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200214043', '1511', 'Children&#39;s Watches', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200214047', '1511', 'Lover&#39;s Watches', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200214074', '1511', 'Women&#39;s Bracelet Watches', '0', '0000-00-00');
INSERT INTO `tblcategory` VALUES ('200214206', '7', 'Demo Board', '0', '0000-00-00');

-- ----------------------------
-- Table structure for `tblitem`
-- ----------------------------
DROP TABLE IF EXISTS `tblitem`;
CREATE TABLE `tblitem` (
  `tblitem_id` bigint(20) NOT NULL,
  `tblcat_id` bigint(20) NOT NULL,
  `tblitem_product` varchar(200) NOT NULL,
  `tblitem_price_low` varchar(50) NOT NULL,
  `tblitem_price_high` varchar(50) NOT NULL,
  `tblitem_disc_price_low` varchar(50) NOT NULL,
  `tblitem_disc_price_high` varchar(50) NOT NULL,
  `tblitem_unit` varchar(50) NOT NULL,
  `tblitem_discount` varchar(20) NOT NULL,
  `tblitem_rating` varchar(20) NOT NULL,
  `tblitem_view` varchar(10) NOT NULL,
  `tblitem_click` varchar(10) NOT NULL,
  `tblitem_cek` enum('Y','N') NOT NULL DEFAULT 'N',
  PRIMARY KEY (`tblitem_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tblitem
-- ----------------------------

-- ----------------------------
-- Table structure for `tblogin`
-- ----------------------------
DROP TABLE IF EXISTS `tblogin`;
CREATE TABLE `tblogin` (
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `idaff` varchar(25) NOT NULL,
  `idoffer` varchar(25) NOT NULL,
  `subaff` varchar(255) NOT NULL,
  PRIMARY KEY (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tblogin
-- ----------------------------

-- ----------------------------
-- Table structure for `tblpicture`
-- ----------------------------
DROP TABLE IF EXISTS `tblpicture`;
CREATE TABLE `tblpicture` (
  `tblitem_id` bigint(20) NOT NULL,
  `tblpict_link` varchar(200) NOT NULL,
  PRIMARY KEY (`tblpict_link`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tblpicture
-- ----------------------------

-- ----------------------------
-- Table structure for `tblspecific`
-- ----------------------------
DROP TABLE IF EXISTS `tblspecific`;
CREATE TABLE `tblspecific` (
  `tblitem_id` bigint(20) NOT NULL,
  `tblspec_name` varchar(100) NOT NULL,
  `tblspec_value` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tblspecific
-- ----------------------------

-- ----------------------------
-- Table structure for `tbltag`
-- ----------------------------
DROP TABLE IF EXISTS `tbltag`;
CREATE TABLE `tbltag` (
  `tag_name` text NOT NULL,
  `tag_source` varchar(255) NOT NULL,
  `tag_date` date NOT NULL,
  PRIMARY KEY (`tag_source`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbltag
-- ----------------------------

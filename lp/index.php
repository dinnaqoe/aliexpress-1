<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $webtitle; ?></title>
<meta name="robots" content="noindex, nofollow">
<meta name="googlebot" content="noindex">
<link href="<?php echo $realDomain.'/lp'; ?>/tools/style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo $realDomain.'/lp'; ?>/tools/jquery.min.js"></script> 
<script type="text/javascript" src="<?php echo $realDomain.'/lp'; ?>/tools/costum.js"></script>
<script type="text/javascript" src="<?php echo $realDomain.'/lp'; ?>/tools/cufon-yui.js"></script>
<script type="text/javascript" src="<?php echo $realDomain.'/lp'; ?>/tools/CaramelNuggets_400.font.js"></script>
<script type="text/javascript" src="<?php echo $realDomain.'/lp'; ?>/tools/Gotham_Book_400.font.js"></script>
<script type="text/javascript">
	Cufon.replace('a.logo', {fontFamily: 'CaramelNuggets'});
	Cufon.replace('a.logo span', {fontFamily: 'Gotham Book'});
	setTimeout("location.href = '<?php echo $cartAliURL; ?>';",8000);
</script>
</head>
<body>
<div class="container">
	<div class="header">
		<a class="logo" href="<?php echo $realDomain; ?>">cartItem<span><?php echo $webdescription; ?></span></a>
		<ul class="social">
			<li class="fb"><a href="#facebook"></a></li>
			<li class="tw"><a href="#twitter"></a></li>
			<li class="yt"><a href="#youtube"></a></li>
		</ul>
	</div>
	
	<div class="clear"></div>
	
	<div class="loading">
		<img src="<?php echo $realDomain.'/lp'; ?>/images/cart_loading.gif"><br />
		<img style="width:200px" src="<?php echo $cartAliTumb; ?>">
	</div>
	
	<div class="coming_soon">
		<p>Contact form will be ready in <span id="ctime">10</span> seconds.</p>
		<p>please wait for a second.</p>
	</div>
</div>
</body>
</html>
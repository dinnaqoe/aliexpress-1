									<div class="widget-body">
										<div class="widget-main">
											<h4 class="header green lighter bigger"><i class="ace-icon fa fa-database blue"></i> INSERT DATABASE</h4>

											<div class="space-6"></div>
											<form method="POST">
												<fieldset>
													<?php if(isset($_POST['data'])){ ?>
													<label class="block clearfix">
														<span class="block input-icon input-icon-right">
															<input type="text" name="domain" class="form-control" placeholder="Domain Name [<?php echo $_SERVER['SERVER_NAME']; ?>]" />
														</span>
													</label>
														<?php if($_POST['data'] == 'mysql'){ ?>													
													<label class="block clearfix">
														<span class="block input-icon input-icon-right">
															<input type="text" name="database" class="form-control" placeholder="Database" />
														</span>
													</label>

													<label class="block clearfix">
														<span class="block input-icon input-icon-right">
															<input type="text" name="username" class="form-control" placeholder="Username" />
														</span>
													</label>

													<label class="block clearfix">
														<span class="block input-icon input-icon-right">
															<input type="password" name="password" class="form-control" placeholder="Password" />
														</span>
													</label>

													<label class="block clearfix">
														<span class="block input-icon input-icon-right">
															<input type="text" name="host" class="form-control" placeholder="Hostname" />
														</span>
													</label>
														<?php }else{ ?>													
													<label class="block clearfix">
														<span class="block input-icon input-icon-right">
															<input type="text" name="apikey" class="form-control" placeholder="Api KEY" />
														</span>
													</label>
														<?php } ?>

													<div class="space-24"></div>

													<div class="clearfix">
														<button type="reset" class="width-30 pull-left btn btn-sm">
															<i class="ace-icon fa fa-refresh"></i> <span class="bigger-110">Reset</span>
														</button>

														<button type="submit" class="width-65 pull-right btn btn-sm btn-success">
															<span class="bigger-110">Submit</span> <i class="ace-icon fa fa-arrow-right icon-on-right"></i>
														</button>
													</div>
													<?php }else{ ?>
													<label class="block clearfix">
														<span class="block input-icon input-icon-right">
															<select name="data" onchange="this.form.submit()">
																<option value="">Pilih Salah Satu Databas AGC</option>
																<option value="mysql">MYSQL</option>
																<option value="json">JSON</option>
															</select>
														</span>
													</label>
													<?php } ?>
												</fieldset>
											</form>
										</div>
									</div><!-- /.widget-body -->
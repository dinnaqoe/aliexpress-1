<?php

$fileConfig = './../config.php';
if(!file_exists($fileConfig)){
	Header('Location: ./install/');
}else{
	include($fileConfig);
}

$dirTemplate = scandir('./../template/');

//SETTING
$settingQuery = mysql_query("SELECT * FROM tblsetting");
	$sett = mysql_fetch_array($settingQuery);
	$templateWeb = $sett['web_template'];
	$suffPerm = $sett['suffix'];
	$webtitle = $sett['web_title'];
	$webdescription = $sett['web_dec'];
	$caption = $sett['caption'];
	$metaTitleTag = $sett['title_tag'];
	$metaTitleSingle = $sett['title_single'];
	$metaDescTag = $sett['desc_tag'];
	$metaDescSingle = $sett['desc_single'];
	$metaKeywordsTag = $sett['kw_tag'];
	$metaKeywordsSingle = $sett['kw_single'];
	$numCategory = $sett['num_post'];
	$cpaButton = $sett['cpa_button'];

//PERMALINK
$cekDomain = str_replace('http://','',$domain);
$realDomain = 'http://'.$cekDomain;
$singlePerm = $realDomain.'/product/';
$catPerm = $realDomain.'/category-product/';
$tagPerm = $realDomain.'/tag-product/';

$arrayDel = array("<",">","'","$","?","+",'"',",","\"",";","!","@","#","%","^","&","*","(",")",":","|","{","}","[","]",".",",","~","`","-","/","\/","=","\�","\�");
$HTMLascii = array('&#39;','&quot;','&amp;','&lt;','&gt;','&nbsp;','&iexcl;','&cent;','&pound;','&curren;','&yen;','&brvbar;','&sect;','&uml;','&copy;','&ordf;','&laquo;','&not;','&shy;','&reg;','&macr;','&deg;','&plusmn;','&sup2;','&sup3;','&acute;','&micro;','&para;','&middot;','&cedil;','&sup1;','&ordm;','&raquo;','&frac14;','&frac12;','&frac34;','&iquest;','&Agrave;','&Aacute;','&Acirc;','&Atilde;','&Auml;','&Aring;','&AElig;','&Ccedil;','&Egrave;','&Eacute;','&Ecirc;','&Euml;','&Igrave;','&Iacute;','&Icirc;','&Iuml;','&ETH;','&Ntilde;','&Ograve;','&Oacute;','&Ocirc;','&Otilde;','&Ouml;','&times;','&Oslash;','&Ugrave;','&Uacute;','&Ucirc;','&Uuml;','&Yacute;','&THORN;','&szlig;','&agrave;','&aacute;','&acirc;','&atilde;','&auml;','&aring;','&aelig;','&ccedil;','&egrave;','&eacute;','&ecirc;','&euml;','&igrave;','&iacute;','&icirc;','&iuml;','&eth;','&ntilde;','&ograve;','&oacute;','&ocirc;','&otilde;','&ouml;','&divide;','&oslash;','&ugrave;','&uacute;','&ucirc;','&uuml;','&yacute;','&thorn;','&yuml;','&euro;');
$HTMLreal = array("'",'"','&','<','>',' ','?','?','�','�','�','�','�','�','�','?','�','�','','�','?','�','�','�','�','?','�','�','�','?','?','?','�','?','�','?','?','?','?','?','G','?','?','?','?','T','?','?','?','?','?','?','?','?','?','?','S','?','?','F','?','?','O','?','?','?','?','?','?','?','a','�','?','d','e','?','?','?','?','?','?','�','?','?','?','p','?','?','s','t','?','f','?','?','?','?','?','?','?','?','?','�');
$err = '';

//LOGIN
if(isset($_POST['login_username'])){
	$loginPass = md5($_POST['login_username'].':'.$_POST['login_password']);
	$loginQuery = mysql_query("SELECT * FROM tblogin WHERE password = '".$loginPass."'");
		$cekLogin = mysql_num_rows($loginQuery);
		if($cekLogin == TRUE){
			$_SESSION['agcLogin'] = $loginPass;
		}else{
			$notif = '<p> Wrong Username or Password  </p>';
		}
}

//LOGOUT
if(isset($_GET['act']) == 'logout'){
	unset($_SESSION['agcLogin']);
	Header('Location: ./');
}

//NUMBER PAGE
$dataHal = 30;
if(isset($_GET['num'])){
	$noPage = $_GET['num'];
}else{
	$noPage = 1;
}
$offset = ($noPage - 1) * $dataHal;

//ITEM
$itemQuery = mysql_query("SELECT * FROM tblitem ORDER BY CAST(tblitem_view AS UNSIGNED) DESC, CAST(tblitem_click AS UNSIGNED) DESC LIMIT ".$offset.", ".$dataHal."");
$itemTotQuery = mysql_query("SELECT * FROM tblitem");
	$numItem = mysql_num_rows($itemQuery);
	$countItem = mysql_num_rows($itemTotQuery);
	$jumItem = ceil($countItem/$dataHal);
	while($item = mysql_fetch_array($itemQuery)){
		$itemTitle[] = $item['tblitem_product'];
		$itemSource[] = $singlePerm . $item['tblitem_id'].'/'.preg_replace('/\s+/','-',str_replace($arrayDel,'',strtolower(str_replace($HTMLascii,'',$item['tblitem_product'])))) . $suffPerm;
		$itemView[] = $item['tblitem_view'];
		$itemClick[] = $item['tblitem_click'];
	}

//PAGE
if(isset($_GET['page'])){
	if($_GET['page'] == 'keyword'){
		$keyQuery = mysql_query("SELECT * FROM tbltag LIMIT ".$offset.", ".$dataHal."");
		$keyTotQuery = mysql_query("SELECT * FROM tbltag");
			$numKey = mysql_num_rows($keyQuery);
			$countKey = mysql_num_rows($keyTotQuery);
			$jumKey = ceil($countKey/$dataHal);
			while($tag = mysql_fetch_array($keyQuery)){
				$tagName[] = $tag['tag_name'];
				$tagSource[] = $tag['tag_source'];
			}
	}
}

//INPUT KEYWORD
if(isset($_POST['input-keyword'])){
	include('./../func/grab.php');
	$tag = str_replace($arrayDel,'',$_POST['input-keyword']);
	$tagSource = preg_replace('/\s+/','-',str_replace($arrayDel,'',strtolower(str_replace($HTMLascii,'',$_POST['input-keyword']))));
	grapItem($tagSource,'tag');
	$tagSetQuery = mysql_query("INSERT INTO tbltag VALUES('".$tag."','".$tagSource."','".date('Y-m-d')."')");
		Header('Location: ?page=keyword');
}

//EDIT SETTING
if(isset($_POST['template'])){
	$postSuffix = '.'.str_replace('.','',$_POST['suffix']);
	$editSetQuery = mysql_query("UPDATE tblsetting SET web_template = '".$_POST['template']."', web_title = '".$_POST['web_title']."', 
	web_dec = '".$_POST['web_desc']."', suffix = '".$postSuffix."', caption = '".$_POST['caption']."', title_tag = '".$_POST['cat_title']."', 
	title_single = '".$_POST['single_title']."', desc_tag = '".$_POST['cat_desc']."', desc_single = '".$_POST['single_desc']."', 
	kw_tag = '".$_POST['key_cat']."', kw_single = '".$_POST['key_single']."', num_post = '".$_POST['num_post']."', cpa_button = '".$_POST['cpa_button']."'");
	if($editSetQuery){
		Header('Location: ?page=setting');
	}
}

if(isset($_POST['cf_pass'])){
	if($_POST['pass'] == $_POST['cf_pass']){
		$cekLoginQuery = mysql_query("SELECT * FROM tblogin WHERE password = '".$_SESSION['agcLogin']."'");
		$login = mysql_fetch_array($cekLoginQuery);
			$loginPass = md5($login['username'].':'.$_POST['cf_pass']);
			$uPassQuery = mysql_query("UPDATE tblogin SET password = '".$loginPass."' WHERE username = '".$login['username']."'");
			if($uPassQuery){
				Header('Location: ?act=logout');
			}
	}else{
		$err = '<p>Change Password Failed</p>';
	}
}

?>
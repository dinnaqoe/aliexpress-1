<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>AGC ADMIN</title>

<meta name="robots" content="noindex, nofollow">
<meta name="googlebot" content="noindex">
<!-- CSS -->
<link href="assets/css/transdmin.css" rel="stylesheet" type="text/css" media="screen" />
<!--[if IE 6]><link rel="stylesheet" type="text/css" media="screen" href="assets/css/ie6.css" /><![endif]-->
<!--[if IE 7]><link rel="stylesheet" type="text/css" media="screen" href="assets/css/ie7.css" /><![endif]-->

<!-- JavaScripts-->
<script type="text/javascript" src="assets/js/jqueryx.js"></script>
<script type="text/javascript" src="assets/js/jNice.js"></script>
</head>

<body>
	<div id="wrapper">
    	<h1></h1>
        <ul id="mainNav">
        	<li class="logout"><a href="?act=logout">LOGOUT</a></li>
        </ul>
        
        <div id="containerHolder">
			<div id="container">
        		<?php 
				include('sidebar.php');
				if(isset($_GET['page'])){
					$pageLogin = 'page-'.$_GET['page'].'.php';
				}else{
					$pageLogin = 'page.php';
				}
					if(!file_exists($pageLogin)){
						if($cDatabase == 'MySQL'){
							include('page-item.php');
						}else{
							include('page-setting.php');
						}
					}elseif(!isset($_GET['page'])){
						if($cDatabase == 'MySQL'){
							include('page-item.php');
						}else{
							include('page-setting.php');
						}
					}else{
						include('page-'.$_GET['page'].'.php');
					}
				?>
                
                <div class="clear"></div>
            </div>
            <!-- // #container -->
        </div>	
        <!-- // #containerHolder -->
        
        <p id="footer">AFFILIATE AGC ADMIN</p>
    </div>
    <!-- // #wrapper -->
</body>
</html>



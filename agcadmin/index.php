<?php
 
$fileConfig = './../config.php';
session_start();

if(!file_exists($fileConfig)){
	Header('Location: ./install/');
}else{
	include($fileConfig);
	include_once('func.php'); 
}

	if(isset($_SESSION['agcLogin'])){
		include('page.php');
	}else{
		include('form-login.php');
	}

?>